[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
    [[!CommonTools? &cmd=`loadChunk` &name=`title`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
    <div class="container">
        [[!CommonTools? &cmd=`loadChunk` &name=`header`]]
        <div class="row">
            <aside class="col-xs-4">
                Sidebar
                [[!CommonTools? &cmd=`loadChunk` &name=`articleSidebar`]] 
            </aside>
            <main id="content" class="col-xs-8" itemscope itemtype="http://schema.org/WebPage">
                <h1 itemprop="name">[[*pagetitle]]</h1>
                <div itemprop="mainContentOfPage">
                    [[!FileLister? 
                        &path=`client-assets/[[*folderPath]]` 
                        &toPlaceholder=`files` 
                        &pathTpl=`fileListerPath` 
                        &fileTpl=`fileListerFile` 
                        &directoryTpl=`fileListerDir`
                        &dateFormat=`%d %b %Y`
                        &sortBy=`[[*sortFilesBy]]`
                        &sortDir=`DESC`
                    ]]

                    [[*content]]
                    <div class="filelist noimgstyle">
                        [[!If?
                            &subject=`[[+files]]`
                            &operator=`EQ`
                            &operand=``
                            &then=``
                            &else=`
                            <table class="fileLister">
                                <tbody>
                                    <tr>
                                        <th class="fileListerName" colspan="2">Name </th>
                                        <th class="fileListerSize" >Filesize</th>
                                        <th class="fileListerDownloads" >Downloads</th>
                                    </tr>
                                    [[+files]]
                                    <tr>
                                        <td colspan="4">Files: [[+filelister.total.files]] </td>
                                    </tr>
                                </tbody>
                            </table>`
                        ]]

                    </div>
            </main>
        </div><!--/.row -->
        [[!CommonTools? &cmd=`loadChunk` &name=`footer`]] 
    </div><!-- /container -->
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]  
</body>
</html>