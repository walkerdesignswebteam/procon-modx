<div id="site-size-helper">
	<h4 class="text-center visible-xs">xs</h4>
	<h4 class="text-center visible-sm">sm</h4>
	<h4 class="text-center visible-md">md</h4>
	<h4 class="text-center visible-lg">lg</h4>
</div>
<header>
	<div class="row">
		<div class="col-xs-12">
			<h1>[[++site_name]]</h1>
		</div>
		<div class="col-xs-12">
			[[Wayfinder? 
			&startId=`0` 
			&level=`1`
			]]
		</div>
	</div>
</header>