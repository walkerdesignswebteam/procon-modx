function StoreMap(mapId,centreLat,centreLon,zoom,style){
    this.mapId = mapId;
    this.lat = centreLat;
    this.lon = centreLon;
    if(zoom !== null){
        this.zoom = zoom;  
    }else{
        this.zoom = 8;
    }
    this.map;
    
    if(style){
        this.style = style;
    }else{
        this.style = '';
    }
    
    //This stuff is related to multiple points
    this.popup = $('#ss_popup');
    this.popupContent = $('#ss_popup_inner');
    this.haveUserLoc = false;
    this.o_stockists;
    this.branchMarkers = [];
    this.you;
    this.stockistMarkers= [];
    this.markerBounds;
    this.makeMap();
};

StoreMap.prototype.makeMap = function(){
var _self = this;
var myOptions = {
             center: new google.maps.LatLng(this.lat, this.lon),
             zoom: _self.zoom,
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             streetViewControl: false,
             mapTypeControl: false,
             
           };
  if(this.style !== ''){
      myOptions.styles = this.style;
  }         
  this.map = new google.maps.Map(document.getElementById(this.mapId), myOptions);
};

StoreMap.prototype.getLocation = function(){
      var _self = this;
    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(function(position){
	    _self.zoom = 12;
	    _self.lat = position.coords.latitude;
	    _self.lon = position.coords.longitude;
	    _self.haveUserLoc = true;
        });
    }
  else{
      //Geo location not supported
        console.log('not using location');
  } 
};

StoreMap.prototype.centreMap = function(lat, lon){
    this.lat = lat;
    this.lon = lon;
    this.map.setZoom(14);
    this.map.setCenter(new google.maps.LatLng(this.lat, this.lon));
};

StoreMap.prototype.addSinglePoint = function(lat, lon, title, markerImg, markerW, markerH, mOriginX, mOriginY, mAnchorX, mAnchorY){
    var image = new google.maps.MarkerImage(markerImg,
        // This marker is 30 pixels wide by 40 pixels tall.
        new google.maps.Size(markerW, markerH),
          // The origin for this image is 0,0.
        new google.maps.Point(mOriginX,mOriginY),
          // The anchor for this image is the base of the middle of the bottom at 0,32.
        new google.maps.Point(mAnchorX, mAnchorY));
        
            var latlng = new google.maps.LatLng(lat, lon);
            var marker = new google.maps.Marker({
                position: latlng,
                map: this.map,
                title: title,
                icon: image
            });
};

StoreMap.prototype.addSinglePointWithInfoWindow = function(lat, lon, title, markerImg, markerW, markerH, mOriginX, mOriginY, mAnchorX, mAnchorY,content){
    var image = new google.maps.MarkerImage(markerImg,
        // This marker is 30 pixels wide by 40 pixels tall.
        new google.maps.Size(markerW, markerH),
          // The origin for this image is 0,0.
        new google.maps.Point(mOriginX,mOriginY),
          // The anchor for this image is the base of the middle of the bottom at 0,32.
        new google.maps.Point(mAnchorX, mAnchorY));
        
            var latlng = new google.maps.LatLng(lat, lon);
            var marker = new google.maps.Marker({
                position: latlng,
                map: this.map,
                title: title,
                icon: image
            });
            this.addInfoWindow(marker,content);
};

StoreMap.prototype.addStockists = function(o_stockists){
	this.o_stockists = o_stockists;
     if(this.stockistMarkers !== null && this.stockistMarkers.lenth > 1){
         this.reAddMarkers(this.stockistMarkers);
     }else{
		 this.addStockistMarkers(this.o_stockists.others);
     }
};

StoreMap.prototype.removeStockists = function(){
    this.removeMarker(this.stockistMarkers);
};

StoreMap.prototype.clearMarkers = function(){
	var length = this.stockistMarkers.length;
	for (var i = 0; i < length; i++ ) {
	    this.stockistMarkers[i].setMap(null);
	}
	this.stockistMarkers= [];
};

StoreMap.prototype.removeMarker =function(a_markersArray){
    var length = a_markersArray.length;
    for(var i = 0; i<length; i++){
        a_markersArray[i].setMap(null);
    }
};

StoreMap.prototype.reAddMarkers =function(a_markersArray){
    var length = a_markersArray.length;
    for(var i=0; i<length; i++){
        a_markersArray[i].setMap(this.map);
    }
};

StoreMap.prototype.addStockistMarkers = function(a_stockists){
        this.clearMarkers();
        this.markerBounds = new google.maps.LatLngBounds();
        
        if(this.haveUserLoc){
    		this.markerBounds.extend(new google.maps.LatLng(this.lat, this.lon));	
        }
        
        var image = new google.maps.MarkerImage(this.markerImg,
        // This marker is 20 pixels wide by 28 pixels tall.
        new google.maps.Size(30, 40),
          // The origin for this image is 0,0.
        new google.maps.Point(0,0),
          // The anchor for this image is the base of the middle of the bottom at 0,32.
        new google.maps.Point(10, 40));
        
            var length = a_stockists.length;
			
            for(i=0; i<length; i++){
				var store = a_stockists[i].store;
			if(store['Latitude'] != 0 && store['Longitude'] != 0){
				var latlng = new google.maps.LatLng(store['Latitude'], store['Longitude']);
	            var marker = new google.maps.Marker({
	                position: latlng,
	                map: this.map,
	                title: store['Company'],
					icon: image
	            });
	
				this.markerBounds.extend(latlng);	
	
	            var content = '<div itemscope itemtype="http://schema.org/Person">'+store['Company']+'</div><div itemscope itemtype="http://schema.org/Place" id="address"><span itemprop="address">'+store['Address']+'</span><br/><br/><b>Phone:</b> <a itemprop="telephone" href=\"tel:'+store['Phone']+'" class="tel">'+store['Phone']+'</a></div><br/><div id=\"stockist_dialog_links\"><div id=\"back_to_map\" class="backToMap" onclick=\"$(\'#popup\').dialog(\'close\')\"><div></div><span>Back to Map</span></div></div>';
	
	            this.addInfoWindow(marker, content,store['Company']);
	            this.stockistMarkers.push(marker);
            }
        }
        
        if(!this.markerBounds.isEmpty()){
	        this.map.setCenter(this.markerBounds.getCenter());
			this.map.fitBounds(this.markerBounds);
        }

};

StoreMap.prototype.addInfoWindow =function(marker, content){
    var _self = this;
    var infowindow = new google.maps.InfoWindow({
        content: content
    });
    google.maps.event.addListener(marker, 'click', function(){
        infowindow.open(_self.map,marker);
    });
};